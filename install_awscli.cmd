https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html


need python installed first (refer to install_python) 

install cli through pip3			https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html

-- yum install -- 
 yum install -y aws-cli

by default it will install in /usr/bin


check awscli in path:
 which aws 

--setup cli key--

 aws configure

--s3 bucket check--
 aws s3 ls
 
 
 --for python already installed --   change the python3 direcotry to reference 
 
 sudo /usr/bin/python3 ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws