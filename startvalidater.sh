1. Download `shareledger` , `slcli` from https://github.com/ShareRing/Shareledger/releases/tag/v1.1.0
2. Run this script, it wil start a validator and start to generating new block
-------------------------
#!/usr/bin/env bash
rm -rf ~/.shareledger
rm -rf ~/.slcli
./shareledger init test --chain-id=testing
./slcli config output json
./slcli config indent true
./slcli config trust-node true
./slcli config chain-id testing
./slcli config keyring-backend test
echo "valve wash okay biology tissue term fire cross solid sword bulb right team enact raven rare frog repeat dust when zebra focus task voice" |./slcli keys add authority --recover --index 0
echo "base cloud render brick switch venture junior exile bounce trumpet surge bulb wrong toilet buzz express detail album basket palm thrive border soon yellow" |./slcli keys add treasurer --recover --index 0
echo "estate year person wink almost pen athlete book lend estate wrong pond boy climb include arrange manual bless install family hood leg police coyote" |./slcli keys add validator --recover --index 0
echo "file holiday trigger clap rebel gown accident sleep illness sauce glide pattern youth tissue rabbit bargain amateur cabbage debris logic there moon inspire ketchup" |./slcli keys add account-operator --recover --index 0
echo "genius hint lemon loan arrive account typical wood similar club below pride horse kidney van fault clarify loud marriage fuel imitate brother jazz swamp" |./slcli keys add issuer --recover --index 0
echo "obscure fetch jelly tissue topple fuel curious avoid isolate render basic cube one swamp squirrel remember autumn jeans emotion joke shoulder turkey busy speed" |./slcli keys add idsigner --recover --index 0
./shareledger add-genesis-authority $(./slcli keys show authority -a)
./shareledger add-genesis-treasurer $(./slcli keys show treasurer -a)
./shareledger add-genesis-validator $(./slcli keys show validator -a)
./shareledger add-genesis-account-operator $(./slcli keys show account-operator -a)
./shareledger add-genesis-account $(./slcli keys show authority -a) 10000000000000000shr
./shareledger add-genesis-account $(./slcli keys show treasurer -a) 10000000000000000shr
./shareledger add-genesis-account $(./slcli keys show validator -a) 10000000000000000shr
./shareledger add-genesis-account $(./slcli keys show account-operator -a) 10000000000000000shr
./shareledger add-genesis-account $(./slcli keys show issuer -a) 10000000000000000shr
./shareledger add-genesis-account $(./slcli keys show idsigner -a) 10000000000000000shr
./shareledger gentx --name "validator" --keyring-backend test --amount 10000000000000000shr
echo "Collecting genesis txs..."
./shareledger collect-gentxs
echo "Validating genesis file..."
./shareledger validate-genesis
./shareledger start