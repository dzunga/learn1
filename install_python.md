UBUNTU:  sudo apt install python3.8

DEBIAN  basic version through yum : sudo yum install python3

Pre-req
	sudo yum install -y  gcc openssl-devel bzip2-devel libffi-devel wget 
	
download package	
	cd /opt
	sudo wget https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tgz
	
	-- 
	https://www.python.org/ftp/python/3.9.4/Python-3.9.4.tgz
	
Extract downloaded pckage 

	sudo tar xzf Python-3.8.2.tgz


setup Python-3


	cd Python-3.8.2
	sudo ./configure --enable-optimizations
	sudo make altinstall

run
   python3.8 -V

Add to PATH: 
   edit ~/.bash_profile 
   
   add to end of PATH : /usr/local/bin
   run to referesh:  . .bash_profile
   
